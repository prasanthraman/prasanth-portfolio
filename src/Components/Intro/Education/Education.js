import React from 'react'

export default function Education() {
    return (
        <div className='text-wrap'>
            <h3> <i className="fa-solid fa-angles-right"></i> Education</h3>
            <div className='row  gy-2 list'>

                <div className='col-12 row'>
                    <i className="fa-solid fa-graduation-cap col-1"></i>
                    <p className='col-10'>  <strong>B.Tech</strong> in Electronics and Instrumentation Engineering</p>

                    <div className='row edu-info d-flex flex-wrap flex-md-column flex-lg-row'>
                        <p className='col-lg-6 '> Pondicherry Engineering College</p>
                        <p className='col-lg-3 '> CGPA - 8.53 </p>
                        <p className='col-lg-3 '> 2017 - 2021</p>
                    </div>

                </div>

                <div className='col-12 row'>
                    <i className="fa-solid fa-school col-1"></i>
                    <p className='col-10'>  <strong>Higher Secondary Certificate</strong></p>

                    <div className='row edu-info d-flex flex-wrap flex-md-column flex-lg-row'>
                        <p className='col-lg-6'>Amalorpavam Higher secondary School</p>
                        <p className='col-lg-3'> Score - 90.1% </p>
                        <p className='col-lg-3'> 2017</p>
                    </div>

                </div>


            </div>
        </div>
    )
}
