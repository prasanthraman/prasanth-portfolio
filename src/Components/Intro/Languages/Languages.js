import React from 'react'

export default function Languages() {
    return (
        <div>
            <h3> <i className="fa-solid fa-angles-right"></i> Languages</h3>
            <ol className='list p-3 d-flex justify-content-around flex-lg-row flex-md-column flex-wrap'>
                <li>
                    English - fluent
                </li>
                <li >
                    Tamil - native
                </li>
            </ol>

        </div>
    )
}
