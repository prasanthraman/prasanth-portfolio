import React from 'react'
import { useState } from 'react';
import img from './prasanth.jpg'
import "./Intro.css"
import Contact from './Contact/Contact'
import Education from './Education/Education'
import WorkExperience from './WorkExperience/WorkExperience'
import Skills from './Skills/Skills'
import Languages from './Languages/Languages'
import Hobbies from './Hobbies/Hobbies'
import { Modal, Button } from 'react-bootstrap';

export default function Intro() {

    const [showModal, setShowModal] = useState(false);

    const handleModalShow = () => {
        setShowModal(true);
    };

    const handleModalClose = () => {
        setShowModal(false);
    };



    return (
        <div className='Intro row m-0'>
            <div className='side-div m-0 p-0 col-md-4 col-sm-12'>
                <h2 className='mt-3'>Prasanth Raman</h2>
                <img 
                    src={img}
                    alt='Prasanth'
                    className='profile-picture mt-2'
                    onClick={handleModalShow} />
                <div className='row  m-0 gy-3 text-wrap'>
                    <Contact />
                    <Education />
                    <Languages />
                    <Hobbies />
                   

                </div>

            </div>
            <div className='main-div my-3 col-md-8 col-sm-12'>
                <h3> <i className="fa-regular fa-circle-user"></i> About me </h3>
                <p> Engineering graduate with a lot of passion towards technology and innovation.</p>
                <hr />
                <WorkExperience />
                <hr />
                <Skills />
                <hr />
            </div>

            <Modal
                show={showModal}
                onHide={handleModalClose} 
                centered
                dialogClassName='profile-picture-modal' >

                <Modal.Body>
                    <img src={img} alt='Prasanth' width="100%" className='modal-profile-picture' />
                </Modal.Body>

                <Modal.Footer>
                    <Button variant='primary' >
                        <a href={img} className='text-white text-decoration-none' download>
                            Download <i className="fa-solid fa-circle-down  fw-lighter fs-6  "></i>

                        </a>
                    </Button>
                    <Button variant='secondary' onClick={handleModalClose}>
                        Close
                    </Button>
                </Modal.Footer>

            </Modal>


        </div>
    )
}
