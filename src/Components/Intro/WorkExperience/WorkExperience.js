import React from 'react'

export default function WorkExperience() {
    return (
        <div>
            <h3><i className="fa-solid fa-briefcase"></i> Work Experience</h3>
            <ul>
                <li>
                    <p><strong>Accenture:</strong> Application Development Associate </p>
                    <div>
                        <strong>Roles and Responsibilities:</strong>
                        <ul className='roles'>
                            <li >
                                Perform <strong>compliance reviews</strong>  on transactions for a US based pharmaceutical client.
                            </li>
                            <li >
                                Quality Assurance trainee and successfully completed Accenture tech express program on <strong>Testing stream</strong>.
                            </li>

                        </ul>

                    </div>
                    <p> <strong> Duration:</strong> From September, 2021 to November, 2022</p>
                </li>
                <hr></hr>
                <li>
                    <p><strong>Maroon Insights: </strong>Full stack Intern</p>

                    <div>

                        <strong>Roles and Responsibilities:</strong>
                        <ul className='roles'>
                            <li >
                                Develop responsive websites as Intern with technologies like <strong> React, Node.js</strong>
                            </li>
                            <li >
                                API integeration for internal site with <strong>python</strong> and its libraries like boto3, pymongo.
                            </li>
                        </ul>

                    </div>

                    <p> <strong> Duration:</strong> From March, 2021 to August, 2021</p>
                </li>
            </ul>
        </div>
    )
}
