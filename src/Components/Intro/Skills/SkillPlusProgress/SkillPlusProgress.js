import React, { Component } from 'react'

export default class SkillPlusProgress extends Component {
    render() {

        
        return (
            <>
                <div className="d-flex  mb-3">
                    <div className="w-25 progressBar mr-3">{this.props.language}</div>
                    <div className="w-50">
                        <div className="progress">
                            <div
                                className="progress-bar "
                                role="progressbar"
                                style={{ width: `${this.props.proficiency}%` }}
                                aria-valuenow={this.props.proficiency}
                                aria-valuemin="0"
                                aria-valuemax="100"
                            ></div>
                        </div>
                    </div>
                </div>
            </>
        )

    }
}

