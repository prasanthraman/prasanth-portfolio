import React from 'react'
import SkillPlusProgress from './SkillPlusProgress/SkillPlusProgress'

export default function Skills() {
    return (
        <div>
            <h3> <i className="fa-solid fa-screwdriver-wrench"></i> Skills </h3>
            <div className="container my-2">
                <div className="row gy-2 d-flex justify-content-around ">
                    <div className="col-md-5 col-sm-12 custom-card" >

                        <h3 className='text-primary'>Programming Languages</h3>
                            <SkillPlusProgress language="JavaScript" proficiency={90}/>
                            <SkillPlusProgress language="Python" proficiency={75}/>
                            <SkillPlusProgress language="C" proficiency={69}/>
                            <SkillPlusProgress language="Go" proficiency={30}/>
                    </div>

                    <div className="col-md-5  col-sm-12 custom-card ">
                        <h3 className='text-primary'>Frameworks</h3>
                        <SkillPlusProgress language="React" proficiency={80}/>
                        <SkillPlusProgress language="Angular" proficiency={25}/>
                    </div>

                    <div className="col-md-5  col-sm-12 custom-card">

                        <h3 className='text-primary'>Libraries</h3>
                        <SkillPlusProgress language="Express" proficiency={80}/>
                        <SkillPlusProgress language="Node.js and NPM" proficiency={50}/>
                   
                        
                    </div>
                    <div className="col-md-5  col-sm-12 custom-card">
                        <h3 className='text-primary'>Others</h3>
                        <SkillPlusProgress language="git" proficiency={80} />
                        <SkillPlusProgress language="MongoDB" proficiency={60} />
                        <SkillPlusProgress language="Linus/Unix OS" proficiency={80} />
                        
                    </div>
                </div>
            </div>
        </div>
    )
}
