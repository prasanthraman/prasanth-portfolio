import React from 'react'

export default function Contact() {
    return (
        <div >
            <h3> <i className="fa-solid fa-angles-right"></i> Contact</h3>
            <div className='row gy-2 p-0 list'>
                <div className='col-12 text-dark row'>
                    <i className="fa-solid fa-envelope col-1"></i>
                    <a href='mailto:prasanth232000@pec.edu' className='col-10 mb-2 text-dark text-decoration-none'>prasanth232000@pec.edu</a>
                </div>

                <div className='col-12 text-dark row '>
                    <i className="fa-solid fa-mobile-retro col-1"></i>
                    <p className='col-10'>8838191804</p>
                </div>

                <div className='col-12 text-dark row '>
                    <i className="fa-solid fa-location-dot col-1"></i>
                    <p className='col-10'>Puducherry, India</p>
                </div>
            </div>
        </div>
    )
}
