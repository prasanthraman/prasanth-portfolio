import React from 'react'

export default function Hobbies() {
    return (
        <div>
            <h3> <i className="fa-solid fa-angles-right"></i> Hobbies</h3>
            <ul className='list p-3 d-flex justify-content-around flex-lg-row flex-md-column flex-wrap'>
                <li>
                    Chess
                </li>
                <li>
                    Cycling
                </li>
            </ul>

        </div>
    )
}
