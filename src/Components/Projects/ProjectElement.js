import React from 'react'

export default function ProjectElement(record,startIdx,index) {
    return (
        <>
            <td>{startIdx+index+1}</td>
            <td>{record.name}</td>
            <td>{record.technologies.map((tech,index)=>{
                if(index === record.technologies.length-1){
                    return <span key={index}>{`and ${tech}. `}</span>
                }
                return <span key={index}>{`${tech}, `}</span>
            })}</td>
            <td>{record.description}</td>
            <td>
                <ul >
                    <li>
                        <a href={record.projectLink} target={`_blank`} >View project</a>
                    </li>
                    <li>
                        <a href={record.sourceLink} target={`_blank`}>View Source - GitLab</a>
                    </li>
                </ul>

            </td>
        </>
    )
}
