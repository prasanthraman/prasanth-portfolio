import React from 'react'
import ProjectElement from './ProjectElement'
import { useState } from 'react';
import ReactPaginate from 'react-paginate';


import "./projects.css"
export default function Projects() {
    const projects = [
        {
            id: 1,
            name: `Fake Store - E-Commerce site`,
            technologies: ['React', 'Node.js', 'Bootstrap', 'Web APIs'],
            description: `A demo E-commerce site built using react`,
            projectLink: `https://fake-store-react-beryl.vercel.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/fake-store-react',
        },
        {
            id: 2,
            name: `Friends Memory game`,
            technologies: ['Html', 'CSS', 'Javascript'],
            description: `Memory game where multiple cards are created based on user's input and users should match two similar cards`,
            projectLink: `https://2nd-benchers.vercel.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/memory-game-with-bonus',
        },
        {
            id: 3,
            name: `Easy Bank landing page`,
            technologies: ['Html', 'CSS', 'flexbox', 'Javascript'],
            description: `Landing page of easy bank was created with responsive design usign flexbox`,
            projectLink: `https://prasanth-easybank-landing-page.vercel.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/easybank-landing-page',
        },
        {
            id: 4,
            name: `	React form validation`,
            technologies: ['React', 'Node.js', 'Bootstrap', `validation npm package`],
            description: `Server side form validation was done using react. Dynamic error messages appear when user enters wrong data and disappears when correct information is given.`,
            projectLink: `https://zingy-cranachan-7f9755.netlify.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/signup-react/',
        },
        {
            id:5,
            name: `Responsive Pricing page `,
            technologies: ['Html', 'CSS', 'flexbox', 'Javascript'],
            description: `Responsive priceing page with toggle button to display pricing as per monthly or anual subscription`,
            projectLink: `https://pricing-html-a32yx9h22-prasanthraman.vercel.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/pricing-html-css',

        },
        {
            id:6,
            name: ` React Social Media Card `,
            technologies: ['React', 'Node.js', 'Bootstrap', `validation npm package`],
            description: `Social media card built with react`,
            projectLink: `https://snazzy-douhua-f8a844.netlify.app/`,
            sourceLink: 'https://gitlab.com/prasanthraman/social-media-card',

        }
    ]


    const itemsPerPage = 5
    const [searchTerm, setSearchTerm] = useState('')
    const [currentPage, setCurrentPage] = useState(0)
    const startIdx = currentPage * itemsPerPage;
    const endIdx = startIdx + itemsPerPage;
    
    const handleSearch = (event) => {
        setSearchTerm(event.target.value)
    }
    
    const filteredProjects = projects.filter((record) => {
        const values = Object.values(record).join(' ').toLowerCase();
        return values.includes(searchTerm.toLowerCase());
    })
    
    const paginatedProjects = filteredProjects.slice(startIdx, endIdx);

      const handlePageChange = (selectedPage) => {
        setCurrentPage(selectedPage.selected);
      }

    return (
        <div className='projects'>
            <div className='custom-container'>
                <div className='row p-0 m-0 bg-dark justify-content-end'>
                    <div className='col-xs-8 col-md-4'>
                        <div className='form-group'>
                            <div className='input-group'>
                                <div className='input-group-prepend'>
                                    <span className='input-group-text'><i className='fa fa-search'></i></span>
                                </div>
                                <input
                                type='text'
                                placeholder='Search'
                                name='filterProjects'
                                className='form-control bg-transparent text-white'
                                onChange={handleSearch}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <table className='table table-striped table-hover table-responsive-md table-sm'>
                    <thead >
                        <tr className='table-dark'>
                            <th>No. </th>
                            <th>Name </th>
                            <th>Technologies used</th>
                            <th>Description</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        {paginatedProjects.map((record,index) => {
                            return <tr key={record.id}>{ProjectElement(record,startIdx,index)}</tr>
                        })}
                    </tbody>
                </table>

                    {/* Pagination */}
                    <div className='pagination-container'>

                        <ReactPaginate
                        previousLabel={<i className="fa-solid fa-arrow-left"></i>}
                        nextLabel={<i className="fa-solid fa-arrow-right"></i>}
                        breakLabel={'...'}
                        pageCount={Math.ceil(filteredProjects.length / itemsPerPage)}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={handlePageChange}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                        />
                    </div>

            </div>
        </div>
    )
}
