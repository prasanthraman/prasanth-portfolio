import React from 'react'

export default function Success() {
    return (
        <div className='custom-form'>
            Success! Your message has been delivered to me.
        </div>
    )
}
