import React from 'react'

export default function Error() {
    return (
        <div className='custom-form'>
            <p>Oops! There was an error and I could not recive your message! Please write to <strong>prasanth232000@pec.edu</strong> while I try to fix this!.</p>

        </div>
    )
}
