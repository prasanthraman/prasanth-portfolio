import React, { Component } from 'react'
import axios from 'axios'

import "./Contact.css"
import Loader from '../Loader/Loader'
import Success from './Success'
import Error from './Error'


export default class ContactPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            details: {},
            errors: {},
            hasErrors: false,
            apiStatus: 'not-initiated', //not-initiated,waiting,success,error
            formValidated: false,
        }
    }
    onSubmit(event) {
        event.preventDefault()
        console.log(this.state)
        this.validateForm(this.state.details)
    }




    validateForm(details) {
        let errors = {}

        if (details === null || !details.hasOwnProperty("message") || details.message === '' || details.message === undefined || details === null) {
            errors.message = 'This field cannot be empty!'
        }
        if (typeof (details.name) !== 'string' || details.name === "") {
            errors.name = 'This is a required field'
        }

        if (JSON.stringify(errors) !== `{}`) {
            this.setState({
                hasErrors: true,
                errors: errors
            })
        } else {
            this.setState({
                hasErrors: false,
                errors: {},
                formValidated: true,
                apiStatus: 'waiting'
            })

            axios.post('https://prasanth-backend-service.onrender.com/send-email',
                {
                    name: this.state.details.name,
                    email: this.state.details.email,
                    message: this.state.details.message,
                }
            )
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        apiStatus: 'success',
                    })
                })
                .catch(error => {
                    console.error(error)
                    this.setState({
                        apiStatus: 'error',
                    })
                })

        }

    }

    render() {
        return (
            <div className='ContactPage'>
                {this.state.formValidated ?
                    this.state.apiStatus === 'waiting' ?
                        <div className='custom-form'> <Loader /> </div> : this.state.apiStatus === 'success' ?
                            <Success /> : <Error /> :
                    <div className='custom-form'>
                        <form className='mt-3 row form  contacts-container gy-4'>

                            {/* Name */}
                            <div className='col-md-8 form-group'>
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <label htmlFor="name" className="input-group-text bg-dark text-white">Name</label>
                                    </div>

                                    <input
                                        className={`${this.state.errors.hasOwnProperty("name") && 'border border-danger'} form-control `}
                                        type={'text'}
                                        id="name"
                                        placeholder='Please enter your name'
                                        onInput={(event) => {
                                            const { name, ...restErrors } = this.state.errors
                                            this.setState({
                                                details: {
                                                    ...this.state.details,
                                                    name: event.target.value,

                                                },
                                                errors: restErrors,
                                            })

                                        }
                                        } />
                                </div>
                                <span className='text-danger'>{this.state.hasErrors && this.state.errors.name}</span>

                            </div>

                            {/* Email */}
                            <div className='col-md-8  form-group'>
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <label htmlFor="email" className="input-group-text bg-dark text-light">Email</label>
                                    </div>
                                    <input
                                        className='form-control '
                                        type={'email'}
                                        id="email"
                                        placeholder='Please enter your email'
                                        onInput={(event) => {

                                            this.setState({
                                                details: {
                                                    ...this.state.details,
                                                    email: event.target.value,
                                                }
                                            })
                                        }} />
                                </div>
                            </div>

                            {/* Message */}
                            <div className='col-md-8 form-group'>
                                <div className="input-group">
                                    <div className="input-group-prepend d-none d-lg-block">
                                        <label htmlFor="message" className="input-group-text message bg-dark text-light">Message</label>
                                    </div>
                                    <textarea
                                        className={`${this.state.errors.hasOwnProperty("message") && 'border border-danger'} form-control  message`}
                                        type={'text'}
                                        id="message"
                                        placeholder='Leave a message'
                                        onInput={(event) => {
                                            const { message, ...restErrors } = this.state.errors
                                            this.setState({
                                                details: {
                                                    ...this.state.details,
                                                    message: event.target.value,
                                                },
                                                errors: restErrors,
                                            })
                                        }} />
                                </div>
                                <span className='text-danger'>{this.state.hasErrors && this.state.errors.message}</span>
                            </div>


                            {/* Submit */}

                            <button
                                onClick={(event) => {
                                    this.onSubmit(event)
                                }}
                                className='form-submit btn btn-dark col-5'> Submit
                            </button>
                        </form>

                    </div>
                }
                        
                        <div className='footer container'>
                               <a href='https://www.facebook.com/prasanth232' id='fb' target={'_blank'} rel="noopener noreferrer"> <i className='fab fa-facebook-f'></i> </a>
                               <a href='https://www.instagram.com/iamprasanthr/' id='insta' target={'_blank'} rel="noopener noreferrer"> <i className="fa-brands fa-instagram"></i></a> 
                               <a href='https://www.linkedin.com/in/prasanth-raman-489b77a4/' id="linkedIn" target={'_blank'} rel="noopener noreferrer"> <i className="fa-brands fa-linkedin"></i></a> 
                               <a href='https://gitlab.com/prasanthraman' target={'_blank'} id="gitlab" rel="noopener noreferrer"> <i className="fa-brands fa-gitlab"></i> </a> 

                        </div>



            </div>
        )
    }
}
