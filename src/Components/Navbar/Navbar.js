import React from 'react'
import { NavLink } from 'react-router-dom'

import "./Navbar.css"

export default function Navbar() {
    return (
        <div>
            <nav className='navbar navbar-expand-lg Navbar'>
                <NavLink className='nav-brand custom-link text-center' to="/"> <h2>  Prasanth Raman  </h2></NavLink>
                <ul className='navbar-nav links'>
                    <li className='nav-item '>
                        <NavLink className="nav-link custom-link" to="/projects"> {`<My-projects />`}</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link custom-link" to="/contact">{`< Contact />`}  </NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    )
}
