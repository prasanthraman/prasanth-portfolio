
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Switch, Route } from 'react-router-dom';

import Navbar from './Components/Navbar/Navbar';
import Intro from './Components/Intro/Intro';
import Projects from './Components/Projects/Projects';
import ContactPage from './Components/ContactPage/ContactPage'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path='/' component={Intro} />
        <Route exact path='/projects' component={Projects} />
        <Route exact path='/contact' component ={ContactPage} />
      </Switch>

    </div>
  )
}

export default App;
